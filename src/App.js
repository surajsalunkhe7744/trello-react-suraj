import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Boards from './Components/boards';
import Lists from './Components/lists'


function App() {
  return (
    <Router>
      <Switch>
        <div className="container-body">
            <Route exact path="/" component={Boards} />
            <Route path="/:id" component={Lists} />
        </div>
      </Switch>
  </Router>

  );
}

export default App;

