import * as action from "../Actions/actionTypes";

export default function reducer(state = {},operation) {
    // console.log(operation.lists.cards[0].idList);
    switch(operation.type) {
        case action.GET_BOARDS :
            return {
                ...state,
                boards:operation.boards,
            }
        case action.ADDED_BOARD :
            return {
                ...state,
                boards: [...state.boards,operation.board]
            }
        case action.GET_LISTS :
            return {
                ...state,
                lists:operation.lists,
            }
        case action.ADDED_LIST :
            return {
                ...state,
                lists: [...state.lists,operation.list]
            }
        case action.REMOVED_LIST :
            return {
                ...state,
                lists: state.lists.filter(list => operation.list.id !== list.id)
            }
        case action.GET_CARDS :
            return {
                ...state,
                lists: state.lists.map(list => {
                    if(list.id === operation.cards.id){
                        return {
                            ...list,
                            cards: operation.cards.response,
                        }
                    }
                    return list;
                })
            }
        case action.ADDED_CARD :
            return {
                ...state,
                lists: state.lists.map(list => {
                    if(list.id === operation.card.id){
                        return {
                            ...list,
                            cards: [...list.cards,operation.card.response]
                        }
                    }
                    return list;
                })
            }
        case action.REMOVED_CARD :
            // return {
            //     ...state,
            //     cards: state.cards.filter(card => operation.card.id !== card.id)
            // }
            return {
                ...state,
                lists: state.lists.map(list => {
                    if(list.id === operation.card.id){
                        // console.log(card.id);
                        console.log(operation.card.response);
                        return {
                            ...list,
                            cards: list.cards.filter(card => card.id !== operation.card.response.idList)
                        }
                    }
                    return list;
                })
            }
        default :
            return state;
    }
}