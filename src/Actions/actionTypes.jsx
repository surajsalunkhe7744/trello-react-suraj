export const GET_BOARDS = "getBoardsData";
export const ADDED_BOARD = "addBoardData";
export const GET_BOARD_NAME = "boardName"

export const GET_LISTS = "getListsData";
export const ADDED_LIST = "addListData";
export const REMOVED_LIST = "removedListData";

export const GET_CARDS = "getCardsData";
export const ADDED_CARD = "addCardData";
export const REMOVED_CARD = "removedCardData";

