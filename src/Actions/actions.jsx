import * as actions from './actionTypes'

export const getBoardsData = (response) => {
    return {
        type: actions.GET_BOARDS,
        boards: response,
    }
}

export const addBoardData = (response) => {

    return {
        type: actions.ADDED_BOARD,
        board: response,
    }
}

export const getListsData = (response) => {
    return {
        type: actions.GET_LISTS,
        lists: response,
    }
}

export const addListData = (response) => {
    return {
        type: actions.ADDED_LIST,
        list: response
    }
}

export const removedListData = (response) => {
    return {
        type: actions.REMOVED_LIST,
        list: response,
    }
}

export const getCardsData = (response,id) => {
    // console.log(response);
    // console.log(id);
    return {
        type: actions.GET_CARDS,
        cards: {response,id}
    }
}

export const addCardData = (response,id) => {
    return {
        type: actions.ADDED_CARD,
        card: {response,id}
    }
}

export const removedCardData = (response,id) => {
    return {
        type: actions.REMOVED_CARD,
        card: {response,id},
    }
}