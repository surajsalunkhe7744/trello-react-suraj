import axios from "axios";

const baseURL = "https://api.trello.com/1/";
const keyAndToken = "key=e69219b803eadf7aa7640230bee11d73&token=ATTA0673dc3799df1237035759fdc8984b8cb15919c2c345710b3e2e985a4d3881336A6D46F1";

export let getBoards = async () => {
    try {
        let response = await axios.get(`${baseURL}members/me/boards?${keyAndToken}`);
        return response.data;
    }
    catch(err) {
        return false;
    } 
}

export let createBoard = async (name) => {
    try {
        let response = await axios.post(`${baseURL}boards/?name=${name}&${keyAndToken}`);
        return response.data;
    }
    catch(err) {
        return false;
    }
}

export let getLists = async (id) => {
    try {
        let response = await axios.get(`${baseURL}boards/${id}/lists?${keyAndToken}`);
        return response.data;
    }
    catch(err) {
        return false;
    } 
}

//https://api.trello.com/1/boards/63ce1c720ffc72030ff210a4/lists?name=${this.state.newListName}&key=e69219b803eadf7aa7640230bee11d73&token=ATTA0673dc3799df1237035759fdc8984b8cb15919c2c345710b3e2e985a4d3881336A6D46F1
export let createList = async (id,name) => {
    try {
        let response = await axios.post(`${baseURL}boards/${id}/lists?name=${name}&${keyAndToken}`);
        return response.data;
    }
    catch(err) {
        return false;
    }
}

//https://api.trello.com/1/lists/${id}/closed?key=e69219b803eadf7aa7640230bee11d73&token=ATTA0673dc3799df1237035759fdc8984b8cb15919c2c345710b3e2e985a4d3881336A6D46F1
export let deleteList = async (id) => {
    try {
        let response = await axios.put(`${baseURL}lists/${id}/closed?${keyAndToken}`,{value:true});
        return response.data;
    }
    catch(err) {
        return false;
    }
}

// https://api.trello.com/1/lists/${this.props.list}/cards?key=e69219b803eadf7aa7640230bee11d73&token=ATTA0673dc3799df1237035759fdc8984b8cb15919c2c345710b3e2e985a4d3881336A6D46F1
export let getCards = async (list) => {
    try {
        let response = await axios.get(`${baseURL}lists/${list}/cards?${keyAndToken}`);
        return response.data;
    }
    catch(err) {
        return false;
    } 
}

// https://api.trello.com/1/cards?name=${this.state.newCardName}&idList=${this.props.list}&key=e69219b803eadf7aa7640230bee11d73&token=ATTA0673dc3799df1237035759fdc8984b8cb15919c2c345710b3e2e985a4d3881336A6D46F1
export let createCard = async (id,name) => {
    try {
        let response = await axios.post(`${baseURL}cards?idList=${id}&name=${name}&${keyAndToken}`);
        return response.data;
    }
    catch(err) {
        return false;
    }
}

//https://api.trello.com/1/cards/${id}?key=e69219b803eadf7aa7640230bee11d73&token=ATTA0673dc3799df1237035759fdc8984b8cb15919c2c345710b3e2e985a4d3881336A6D46F1
export let deleteCard = async (id) => {
    try {
        let response = await axios.delete(`${baseURL}cards/${id}?${keyAndToken}`);
        return response.data;
    }
    catch(err) {
        return false;
    }
}
