import React, { Component } from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import CloseButton from "react-bootstrap/CloseButton";
import { connect } from 'react-redux'

import Navbar from "./navbar";
import CardGroup from "./cardGroup";
import { getListsData,addListData,removedListData } from "../Actions/actions";
import { getLists, createList, deleteList } from "./apiLinks";

class Lists extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newListName: "",
      isError: false,
      isLoaded: true,
    };
  }

  componentDidMount = async () => {
    let response = await getLists(this.props.match.params.id);
    response
      ? this.props.getListsData(response)
      : this.setState({
          isError: true,
        });
      this.setState({
        isLoaded: false,
      })
  };

  newListName = (e) => {
    this.setState({
      newListName: e.target.value,
    });
  };

  createNewList = async (e) => {
    e.preventDefault();
    if (this.state.newListName !== "") {
      let response = await createList(
        this.props.match.params.id,
        this.state.newListName
      );
      response
        ? this.props.addListData(response)
        : this.setState({
            isError: true,
          });
        this.setState({
          newListName: "",
        });
    }
  };

  deleteListItem = async (id) => {
    let response = await deleteList(id);
    response
      ? this.props.removedListData(response)
      : this.setState({
          isError: true,
        });
  };

  render() {
    let { isError, isLoaded } = this.state;
    let lists = this.props.lists;
    return (
      <div>
        {isLoaded && isError === false && (
          <h1 className="lists-validation-message">Loading ... </h1>
        )}
        {isError === false && isLoaded === false && lists.length === 0 && (
          <div>
            <Navbar />
            <div className="create-board">
              <form onSubmit={this.createNewList}>
                <input
                  type="text"
                  placeholder="Enter list name"
                  className="board-name-input"
                  onChange={this.newListName}
                ></input>
                <Button variant="primary" type="submit">
                  Create
                </Button>
              </form>
            </div>
            <h1 className="lists-validation-message">No Lists Found ...</h1>
          </div>
        )}
        {isError === false && isLoaded === false && lists.length !== 0 && (
          <div>
            <Navbar></Navbar>
            <div className="create-board">
              <form onSubmit={this.createNewList}>
                <input
                  type="text"
                  placeholder="Enter list name"
                  className="board-name-input"
                  onChange={this.newListName}
                ></input>
                <Button variant="primary" type="submit">
                  Create
                </Button>
              </form>
            </div>
            <div className="all-board-cards">
              {lists.map((list) => {
                const { id, name } = list;
                return (
                  <div key={id} className="board-card">
                    <Card border="primary">
                      <Card.Header>
                        <div className="board-card-head">
                          {name}
                          <CloseButton
                            onClick={() => this.deleteListItem(id)}
                          />
                        </div>
                      </Card.Header>
                      <Card.Body>
                        <CardGroup list={id}></CardGroup>
                      </Card.Body>
                    </Card>
                  </div>
                );
              })}
            </div>
          </div>
        )}
        {isError && (
          <h1 className="lists-validation-message">
            Error while fetching lists ...{" "}
          </h1>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) =>{
  return {
    lists: state.lists,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getListsData: (response) => dispatch(getListsData(response)),
    addListData: (response) => dispatch(addListData(response)),
    removedListData: (response) => dispatch(removedListData(response))
  }
} 

export default connect(mapStateToProps,mapDispatchToProps)(Lists);
