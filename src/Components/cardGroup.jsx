import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import CloseButton from "react-bootstrap/CloseButton";
import { connect } from "react-redux"

import { getCardsData, addCardData, removedCardData } from "../Actions/actions";
// import store from "../Store/store";
import { getCards, createCard, deleteCard } from "./apiLinks";

class CardGroup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // cards: [],
      newCardName: "",
      isError: false,
      isLoaded: true,
    };
  }

  componentDidMount = async () => {
    let response = await getCards(this.props.list);
    // store.dispatch(getCardsData(response));
    // console.log(response);
    // console.log(this.props.list);
    response
      ? this.props.getCardsData(response,this.props.list)
      : this.setState({
          isError: true,
        });
      this.setState({
        isLoaded: false,
      })
  };

  newListName = (e) => {
    this.setState({
      newCardName: e.target.value,
    });
  };

  createNewCard = async (e) => {
    e.preventDefault();
    if (this.state.newCardName !== "") {
      let response = await createCard(this.props.list, this.state.newCardName);
      // store.dispatch(addCardData(response));
    // console.log(store.getState());
      response
        ? this.props.addCardData(response,this.props.list)
        : this.setState({
            isError: true,
          });
          this.setState({
            newCardName: "",
          })
    }
  };

  deleteCard = async (id) => {
    let response = await deleteCard(id);
    // store.dispatch(removedCardData(response));
    // console.log(store.getState());
    response
      ? this.props.removedCardData(id,this.props.list)
      : this.setState({
          isError: true,
        });
  };

  render() {
    let { isError, isLoaded } = this.state;
    let list = this.props.lists.filter(list => list.id === this.props.list);
    // console.log(list);
    let cards = list[0].cards;
    // console.log(cards);
    return (
      <div>
        {isLoaded && <h1 className="cards-validation-message">Loading ...</h1>}
        {isError === false && isLoaded === false && (
          <div>
            {cards.map((card) => {
              const { id, name } = card;
              return (
                <div border="primary" className="list-cards" key={id}>
                  {name}
                  <CloseButton onClick={() => this.deleteCard(id)} />
                </div>
              );
            })}
            <div>
              <form onSubmit={this.createNewCard}>
                <input
                  placeholder="Add Card"
                  className="add-card"
                  onChange={this.newListName}
                ></input>
                <Button variant="outline-primary" type="submit">Create</Button>
              </form>
            </div>
          </div>
        )}
        {isError && (
          <h1 className="cards-validation-message">
            Error while fetching Cards ...
          </h1>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) =>{
  return {
    lists: state.lists,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCardsData: (response,id) => dispatch(getCardsData(response,id)),
    addCardData: (response,id) => dispatch(addCardData(response,id)),
    removedCardData: (response,id) => dispatch(removedCardData(response,id))
  }
} 

export default connect(mapStateToProps,mapDispatchToProps)(CardGroup);
