import React, { Component } from "react";
import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { connect } from 'react-redux'

import Navbar from "./navbar";
import { getBoardsData,addBoardData } from "../Actions/actions";
import { getBoards, createBoard } from "./apiLinks";

class Boards extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newBoardName: "",
      isLoaded: true,
      isError: false,
    };
  }

  componentDidMount = async () => {
    let response = await getBoards();
    this.setState({
      isLoaded: false
    })
    response
      ? this.props.getBoardsData(response)
      : this.setState({
          isError: true,
        });
  };

  newBoardName = (e) => {
    this.setState({
      newBoardName: e.target.value,
    });
  };

  createNewBoard = async (e) => {
    e.preventDefault();
    if (this.state.newBoardName !== "") {
      let response = await createBoard(this.state.newBoardName);
      response
        ? this.props.addBoardData(response)
        : this.setState({
            isError: true,
          });
        this.setState({
          newBoardName: "",
        });
    }
  };

  render() {
    let { isError, isLoaded } = this.state;
    const boards = this.props.boards;
    return (
      <div>
        {isLoaded && <h1 className="boards-validation-message">Loading ...</h1>}
        {isError === false && isLoaded === false && boards.length === 0 && (
          <div>
            <Navbar></Navbar>
            <div className="create-board">
              <form onSubmit={this.createNewBoard}>
                <input
                  type="text"
                  placeholder="Enter board name"
                  className="board-name-input"
                  onChange={this.newBoardName}
                ></input>
                <Button variant="primary" type="submit">
                  Create
                </Button>
              </form>
            </div>
            <h1 className="boards-validation-message">No Boards Found ...</h1>
          </div>
        )}
        {isError === false && isLoaded === false && boards.length !== 0 && (
          <div>
            <Navbar></Navbar>
            <div className="create-board">
              <form onSubmit={this.createNewBoard}>
                <input
                  type="text"
                  placeholder="Enter board name"
                  className="board-name-input"
                  onChange={this.newBoardName}
                ></input>

                <Button variant="primary" type="submit">
                  Create
                </Button>
              </form>
            </div>
            <div className="board">
              {boards.map((board) => {
                const { id, name } = board;
                return (
                  <Card style={{ width: "18rem" }} key={id} className="card">
                    <Card.Title>{name}</Card.Title>
                    <Link to={`/${id}`}>
                      <Button variant="outline-primary">Enter</Button>
                    </Link>
                  </Card>
                );
              })}
            </div>
          </div>
        )}
        {isError && (
          <h1 className="boards-validation-message">
            Error, while fetching data ...
          </h1>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) =>{
  return {
    boards: state.boards,
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
      getBoardsData: (response) => dispatch(getBoardsData(response)),
      addBoardData: (response) => dispatch(addBoardData(response))
    }
} 

export default connect(mapStateToProps,mapDispatchToProps)(Boards);


