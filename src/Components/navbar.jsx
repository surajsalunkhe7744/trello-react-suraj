import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from "react-router-dom";

function navbar() {
    return (
        <Navbar bg="dark" variant="dark">
        <Container>
            <Link to={"/"}>
                <Navbar.Brand>
                <img
                alt=""
                src="https://cdn-icons-png.flaticon.com/512/6124/6124991.png"
                width="30"
                height="30"
                className="d-inline-block align-top"
                />{' '}
                Trello
                </Navbar.Brand>
            </Link>
        </Container>
      </Navbar>
    );
}

export default navbar